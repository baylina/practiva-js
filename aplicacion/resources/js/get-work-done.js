var customerList = document.getElementById('customer-list');

// Consulta a Ruben xq no funciona asi
// var activeLi = document.querySelector('#customer-list li.active');

var dniCustomer = document.getElementById('customer-dni');
var nameCustomer = document.getElementById('customer-name');
var surnameCustomer = document.getElementById('customer-surname');
var addressCustomer = document.getElementById('customer-address');
var cityCustomer = document.getElementById('customer-city');
var postcodeCustomer = document.getElementById('customer-postcode');
var provinceCustomer = document.getElementById('customer-province');
var emailCustomer = document.getElementById('customer-email');
var passwordCustomer = document.getElementById('customer-password');
var passwordConfirmCustomer = document.getElementById('customer-password-confirm');
var acceptConditionsCustomer = document.getElementById('customer-accept-conditions');

function ajax(url, method, data, success) {
    var request = new XMLHttpRequest();
    data = data ? JSON.stringify(data) : null;
    request.onreadystatechange = onStateChange;
    request.overrideMimeType('application/json');
    request.open(method, url, true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(data);

    function onStateChange() {
        if (request.readyState < 4) {
            return; // not ready yet
        }
        if (request.status && (request.status < 200 || request.status >= 300)) {
            console.error('Request error', request);
            return;
        }
        // Request is complete and OK
        var result = JSON.parse(request.responseText);
        success(result);
    }
}

function UserListToDOM(customers) {
    for (var i = 0; i < customers.length; i++) {
        addCustomerToList(customers[i], false);
    }
    customerList.addEventListener('click', fillFormWithCustomer, false);
}

function addCustomerToList(customer, active) {
    var aTag, liTag;
    aTag = document.createElement("a");
    aTag.setAttribute("href", "#");
    aTag.setAttribute("id", customer.id);
    aTag.textContent = customer.name + ' ' + customer.surname;
    liTag = document.createElement("li");
    if (active) {
        liTag.setAttribute("class", "active")
    }
    liTag.appendChild(aTag);
    customerList.appendChild(liTag);
}

function updateCustomerToList(customer) {
    document.querySelector('#customer-list li.active').firstChild.textContent = customer.name + ' ' + customer.surname;
    //activeLi.firstChild.textContent = customer.name + ' ' + customer.surname;
}

function loadUsersList() {
    var url = 'https://morning-ridge-3233.herokuapp.com/student/joan-baylina-mele/customers';
    ajax(url, 'GET', null, UserListToDOM);
}

function fillFormWithCustomer(event) {
    if (event.target &&
        event.target.tagName.toUpperCase() == 'A' &&
        event.target.parentNode.className !== 'active' &&
        parseInt(event.target.getAttribute("id"), 10)
    ) {
        var customerURL = 'https://morning-ridge-3233.herokuapp.com/student/joan-baylina-mele/customers/' + event.target.getAttribute("id");
        ajax(customerURL, 'GET', null, editCustomer);

        unselectActiveCustomerFromList();
        event.target.parentNode.classList.toggle('active');
    }
    event.preventDefault();
}

function unselectActiveCustomerFromList() {
    if (document.querySelector('#customer-list li.active') !== null) {
        document.querySelector('#customer-list li.active').classList.remove('active');
    }
}

function removeActiveCustomerFromList() {
    document.querySelector('#customer-list li.active').parentNode.removeChild(document.querySelector('#customer-list li.active'));
}

function clearForm() {
    event.preventDefault();

    dniCustomer.value = '';
    nameCustomer.value = '';
    surnameCustomer.value = '';
    addressCustomer.value = '';
    postcodeCustomer.value = '';
    cityCustomer.value = '';
    provinceCustomer.selectedIndex = 0;
    emailCustomer.value = '';
    passwordCustomer.value = '';
    passwordConfirmCustomer.value = '';
    acceptConditionsCustomer.checked = false;
}

function editCustomer(customer) {
    dniCustomer.value = customer.document;
    nameCustomer.value = customer.name;
    surnameCustomer.value = customer.surname;
    addressCustomer.value = customer.address;
    cityCustomer.value = customer.city;
    postcodeCustomer.value = customer.postcode;
    if (customer.province > 0) {
        provinceCustomer.value = customer.province;
    } else {
        provinceCustomer.selectedIndex = 0;
    }
    emailCustomer.value = customer.email;
    passwordCustomer.value = customer.password;
    passwordConfirmCustomer.value = customer.password_confirmation;
    acceptConditionsCustomer.checked = customer.subscribed_to_bulletin;
}

function loadProvinces() {
    provinceCustomer = document.getElementById('customer-province');
    for (var i = 0; i < SPAIN_PROVINCES.length; i++) {
        var option = document.createElement("option");
        option.text = SPAIN_PROVINCES[i].name;
        option.value = SPAIN_PROVINCES[i].id;
        provinceCustomer.appendChild(option);
    }
}

function prepareInsert() {
    clearForm();
    unselectActiveCustomerFromList();
}

function setListenerToNewCustomerButton() {
    document.getElementById('new-customer-button').addEventListener('click', prepareInsert, false);
    //unselectActiveCustomerFromList();
}

function anyCustomerSelected() {
    return document.querySelector('#customer-list li.active') !== null;
}

function getIdSelectedCustomer() {
    if (anyCustomerSelected() && parseInt(document.querySelector('#customer-list li.active').firstChild.id, 10) > 0) {
        return document.querySelector('#customer-list li.active').firstChild.id;
    }
}

function setListenerToSaveButton() {
    document.querySelector('input[value="Guardar"]').addEventListener('click', saveButtonClicked, false);
}

function setListenerToDeleteButton() {
    document.querySelector('input[value="Eliminar"]').addEventListener('click', deleteButtonClicked, false);
}

function deleteButtonClicked() {
    if (anyCustomerSelected()) {
        var areYouSure = confirm("Sure you want to delete it!");
        if (areYouSure == false) {
            return;
        }
        var url = 'https://morning-ridge-3233.herokuapp.com/student/joan-baylina-mele/customers/' + getIdSelectedCustomer();
        ajax(url, 'DELETE', null, function () {
            clearForm();
            removeActiveCustomerFromList();
        });
    } else {
        alert("No Customer selected")
    }
}

function validateFields(customer) {
    var msg = '';

    var dniMask = /^\d{8}[a-zA-Z]$/;
    var postalCodeMask = /^\d{5}$/;

    if (dniMask.exec(customer.document) == null) {
        msg += "Incorrect DNI format \n";
    }

    if (customer.name.length < 1) {
        msg += "Name Required \n";
    }

    if (customer.surname.length < 1) {
        msg += "Surname Required \n";
    }

    if ((customer.postcode.length > 0) && (postalCodeMask.exec(customer.postcode) == null)) {
        msg += "Incorrect Postal Code format \n";
    }

    if (customer.email.length < 1) {
        msg += "Email Required \n";
    }

    if ((customer.password.length < 6) ||
        (customer.password_confirmation.length < 6) ||
        (customer.password !== customer.password_confirmation)
    ) {
        msg += "Pasword fields must be equal and 5 chars minimum \n";
    }

    return msg;
}

function saveButtonClicked(event) {
    event.preventDefault();
    var customer = {};
    customer.document = document.getElementById('customer-dni').value;
    customer.name = document.getElementById('customer-name').value;
    customer.surname = document.getElementById('customer-surname').value;
    customer.address = document.getElementById('customer-address').value;
    customer.city = document.getElementById('customer-city').value;
    customer.postcode = document.getElementById('customer-postcode').value;
    if ((document.getElementById('customer-province').options[document.getElementById('customer-province').selectedIndex].value) > 0) {
        customer.province = parseInt(document.getElementById('customer-province').options[document.getElementById('customer-province').selectedIndex].value, 10);
    }
    customer.email = document.getElementById('customer-email').value;
    customer.password = document.getElementById('customer-password').value;
    customer.password_confirmation = document.getElementById('customer-password-confirm').value;
    customer.subscribed_to_bulletin = document.getElementById('customer-accept-conditions').checked;

    if (validateFields(customer) !== '') {
        alert(validateFields(customer));
    } else {
        if (anyCustomerSelected()) {
            updateCustomer(customer);
        } else {
            createCustomer(customer);
        }
    }
}

function createCustomer(customer) {
    var url = 'https://morning-ridge-3233.herokuapp.com/student/joan-baylina-mele/customers';
    ajax(url, 'POST', customer, function (response) {
        customer.id = response.id;
        addCustomerToList(customer, true);
    });
}

function updateCustomer(customer) {
    var url = 'https://morning-ridge-3233.herokuapp.com/student/joan-baylina-mele/customers/' + getIdSelectedCustomer();
    ajax(url, 'PUT', customer, function (response) {
        console.log(response)
    });
    updateCustomerToList(customer);
}

function startHavingFun() {
    loadProvinces();
    loadUsersList();
    setListenerToNewCustomerButton();
    setListenerToSaveButton();
    setListenerToDeleteButton();
}

startHavingFun();

